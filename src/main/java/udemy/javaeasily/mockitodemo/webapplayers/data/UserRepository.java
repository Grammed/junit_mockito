package udemy.javaeasily.mockitodemo.webapplayers.data;

import udemy.javaeasily.mockitodemo.webapplayers.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private Map<String, User> users = new HashMap<String, User>();

    public UserRepository() {
        // Regular Users
        users.put("jimbo",  User.createRegularUser("jimbo", "letmein"));
        users.put("frank", User.createRegularUser("frank", "myPassword"));
        users.put("suzie", User.createRegularUser("frank", "abracadabra"));

        // Admin Users

        users.put("suzie", User.createAdminUser("thomas", "tommy"));
    }

    public User findByUsername(String username) {
        return users.get(username);


    }
}
