package udemy.javaeasily.mockitodemo.webapplayers.web;

/*A https request comes in with user name and password, this class either presents user with their home page or reject
* redirect if not correct*/

import udemy.javaeasily.mockitodemo.webapplayers.service.AuthenticationService;

public class LoginController {
    private AuthenticationService authenticationService;

    public LoginController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public String service(String username, String password) {
        return (authenticationService.authenticate(username, password)) ? "/home" : "/login";
    }
}
