package udemy.javaeasily.mockitodemo.webapplayers.service;

import udemy.javaeasily.mockitodemo.webapplayers.User;
import udemy.javaeasily.mockitodemo.webapplayers.data.UserRepository;

public class AuthenticationService {
    private UserRepository userRepository;

    public AuthenticationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean authenticate(String username, String password) {
        User user = userRepository.findByUsername(username);
        return user.getPassword().equals(password);

    }
}
