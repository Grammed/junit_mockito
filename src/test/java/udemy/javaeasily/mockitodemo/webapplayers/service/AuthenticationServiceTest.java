package udemy.javaeasily.mockitodemo.webapplayers.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import udemy.javaeasily.mockitodemo.webapplayers.data.UserRepository;

class AuthenticationServiceTest {
    private AuthenticationService service; // System Under Test
    private UserRepository repository; // instance of mock test class relies upon

    @BeforeEach
    void setUp() throws Exception{
        this.repository = Mockito.mock(UserRepository.class);
        this.service = new AuthenticationService(repository);
    }

    @DisplayName(value = "testAuthenticate ")
    @Test
    void testAuthenticate() {
        //arrange
        Mockito.when(repository.findByUsername(Mockito.anyString())).thenThrow(new IllegalArgumentException());

        //act
        this.service.authenticate("harry", "harry12345");

    }
}