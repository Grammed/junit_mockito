package udemy.javaeasily.mockitodemo.webapplayers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import udemy.javaeasily.mockitodemo.webapplayers.service.AuthenticationService;

import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;

class LoginControllerTest {

    private LoginController controller; // Actual instance of Testable class
    private AuthenticationService service; // mock, we will adjust this for each test


    @BeforeEach
    void setUp() throws Exception{
        this.service =    Mockito.mock(AuthenticationService.class);
        this.controller = new LoginController(this.service);
    }

    @DisplayName(value = "testService valid username and password, logs in user.")
    @Test
    void testService() {
        // arrange
        // Set up AuthenticationService method 'authenticate' to always return true. HAPPY_PATH
        Mockito.when(service.authenticate(Mockito.anyString(), Mockito.anyString())).thenReturn(true);

        // act Assign the return of this "happy path" to viewPath.
        String viewPath = controller.service("William", "password1234");

        //assert
        assertNotNull(viewPath);
        assertEquals("/home", viewPath);

    }
}